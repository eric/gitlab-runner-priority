# GitLab Runner wrapper supporting the concept of priority queues


## Why write a wrapper?

Because gitlab-runner picks the jobs in the order they were created.

This makes sense, but is not what we want: we need to be able to tag certain
jobs as high priority and have them executed before _any_ lower-priority jobs.

Our use-case is pre-merge testing: we want merge requests to be tested at the
time of merging, and also allow everyone else to run their tests when pushing
changes in their forks.
The problem is that when a lot of people are running pipelines in their forks,
they exhaust the resources and merge requests cannot be merged in reasonable
time anymore. This is our solution to this problem, by adding the
`priority:high` tag to jobs in pre-merge pipelines.


## How does it work?

This script runs, in a parallel for each DUT, a loop that will read the
configuration for the DUT for the first priority in the list, check for a job
waiting for the corresponding `priority:$prio` runner tag, and if no job gets
picked up, do the same thing with the next priority queue.

If a job _does_ get picked up at any given priority, at the end of that job the
script goes back to the beginning, checking the highest priority queue.

This ensures that the highest priority job is picked at any given time.

This also means that high-priority jobs can starve lower-priority ones;
this is by design.

## What does it require?

The script itself requires Python 3.11, or Python 3.7 + `pip install tomli`.

Since this is a wrapper for `gitlab-runner`, the latter is also required, and
we use `--max-builds` and `--wait-timeout` which were introduced in `v1.9.0`
(2016-12-22).

## How do I use it?

This script is a drop-in replacement for the gitlab-runner daemon used in the
systemd service.

The priorities are defined in the config file (`--priority-config`) as a list
of labels:
```toml
priority_labels = [
    "high",
    "medium",
    "low",
    "basically-never-gonna-run",
]
```

These labels can be anything you want, and will be prefixed with `priority:`
to form the tag that the runner will pick, and therefore the `tags:` that you
need to put on your job.

Any job that doesn't have a priority label ends up picked by every priority
queue, which is really not what we want, but it allows for a smooth transition
into using this wrapper:
1. enable the wrapper
   -> get priority queues, jobs are picked randomly
2. add priority tags to the jobs
   -> jobs with priority tag are picked by the right queue, jobs without
      continue to be picked randomly
3. enforce presence of a priority tag in the runner `pre_build_script`
   -> everything works perfectly

The config file also contains the timeout between checking each queue; the
lower the timeout, the quicker it cycles through. The minimum is 1 second,
which is the default is unspecified (because 0 means "wait forever" which is
the opposite of what we need here).
```toml
timeout_between_queues = 10 # sec
```

By default, if the runner spends more 10 seconds we consider that it picked
a job and ran it. This can be tweaked using the `min_job_duration` setting.
```toml
min_job_duration = 30 # sec
```

If `--priority-config` is not specified, it defaults to `priority_config.toml`
in the same folder as `gitlab-runner`'s config, which is `/etc/gitlab-runner/`
when running as root, and `$HOME/.gitlab-runner/` when running as a regular
user.


### So what do I actually type?

Register your DUT like this:
```sh
./gitlab_runner_priority.py register \
  --url https://gitlab.freedesktop.org \
  --registration-token GR0123456789abcdef0123456789a \
  --name "$HOSTNAME"
```

Unregister that DUT with:
```sh
./gitlab_runner_priority.py \
  unregister \
  --name "$HOSTNAME"
```

In your systemd service (`/usr/lib/systemd/system/gitlab-runner.service`),
replace the `gitlab-runner` with this script, adding the path to your priority
config if needed. For instance:
```ini
ExecStart=/usr/bin/gitlab-runner "run" "--working-directory" "/var/lib/gitlab-runner" "--config" "/etc/gitlab-runner/config.toml" "--service" "gitlab-runner" "--user" "gitlab-runner"
```
becomes:
```ini
ExecStart=/usr/local/bin/gitlab_runner_priority.py "run" "--working-directory" "/var/lib/gitlab-runner" "--config" "/etc/gitlab-runner/config.toml" "--service" "gitlab-runner" "--user" "gitlab-runner"
```
or:
```ini
ExecStart=/usr/local/bin/gitlab_runner_priority.py --priority-config /path/to/priority_config.toml "run" "--working-directory" "/var/lib/gitlab-runner" "--config" "/etc/gitlab-runner/config.toml" "--service" "gitlab-runner" "--user" "gitlab-runner"
```

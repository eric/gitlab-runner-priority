#!/usr/bin/env python
"""
Wrapper around some of gitlab-runner subcommands, providing the additional
concept of priority queues.

See https://gitlab.freedesktop.org/eric/gitlab-runner-priority for more
informations.
"""

import argparse
import collections
import os
import pathlib
import signal
import subprocess
import time
import concurrent.futures

try:
    import tomllib
except ModuleNotFoundError:  # Python < 3.11
    import tomli as tomllib


PRIORITY_TAG_PREFIX = 'priority:'


def get_gitlab_runner_config_folder() -> str:
    """
    Reproduce gitlab-runner's logic for selecting the config folder,
    so that we can store our config next to it transparently.
    """
    if os.geteuid() == 0:
        return pathlib.Path('/etc/gitlab-runner/')
    return pathlib.Path.home() / '.gitlab-runner/'


def get_runner_config_path(gitlab_runner_args: list[str]) -> dict:
    """
    Detect when a custom gitlab-runner config path is passed, and
    return the default one otherwise.
    """
    for i, arg in enumerate(gitlab_runner_args):
        if arg in {'-c', '--config'}:
            return pathlib.Path(gitlab_runner_args[i + 1])

    return get_gitlab_runner_config_folder() / 'config.toml'


def get_toml_config(config_path: pathlib.Path) -> dict:
    """
    Open up a TOML config file, parse it, and return the resulting dict.
    """
    with config_path.open('rb') as config_file:
        return tomllib.load(config_file)


def get_priority_config(config_path: pathlib.Path) -> dict:
    """
    Load the priority config file, and ensure its content is as expected
    before returning it.
    """
    config = get_toml_config(config_path)

    # `priority_labels` is required:
    # - must be a list of string
    # - each item must appear only once
    assert 'priority_labels' in config
    priority_labels = config['priority_labels']
    assert isinstance(priority_labels, list)
    assert all(isinstance(label, str) for label in priority_labels)
    assert len(priority_labels) == len(set(priority_labels))

    # `timeout_between_queues` is optional, default value is 1
    # - must be a positive number
    config['timeout_between_queues'] = config.get('timeout_between_queues', 1)
    assert config['timeout_between_queues'] >= 1

    # `min_job_duration` is optional, default value is 10
    # - must be a positive number
    config['min_job_duration'] = config.get('min_job_duration', 10)
    assert config['min_job_duration'] >= 1

    return config


def get_argument_name(section: str | None, config_name: str) -> str:
    """
    Format the config name into an argument name.
    """
    prefix = '--'
    if section:
        prefix += f'{section}-'

    match section, config_name:
        case None, 'environment':
            arg_name = 'env'
        case 'docker', 'tls_verify':
            arg_name = 'tlsverify'
        case _, _:
            arg_name = config_name.replace('_', '-')

    return prefix + arg_name


def convert_to_arguments(section: str, name: str, value: any) -> list[str]:
    """
    Convert a config element into the appropriate sequence of arguments.
    """
    arguments = []

    match value:
        case bool():
            # Boolean arguments simply exist or not, they don't have a value.
            if value:
                arguments.append(get_argument_name(section, name))

        # As bools are also ints, `case int` needs to be after `case bool`.
        case int() | str():
            # The simple case: `--name value`
            arguments.append(get_argument_name(section, name))
            arguments.append(str(value))

        case list():
            # Lists become a list of `--name value`, `--name value2`, etc.
            for element in value:
                arguments.append(get_argument_name(section, name))
                arguments.append(str(element))

        case _:
            # If you hit this, please raise an issue upstream :)
            assert False, f'value is of unsupported type {type(value)}: {value}'

    return arguments


def get_arguments_for_runner(runner: dict) -> list[str]:
    """
    Parse all the config elements for a runner and return the corresponding
    sequence of arguments.
    """
    arguments = []

    for key, value in runner.items():
        match key, value:
            # We only support docker executors for now.
            case 'docker', _:
                for docker_key, docker_value in value.items():
                    arguments.extend(convert_to_arguments('docker',
                                                          docker_key,
                                                          docker_value))

            # `limit` (parallel jobs on this runner) needs to be 1 for
            # this script to work.
            case 'limit', _:
                assert value == 1
                arguments.extend(['--limit', '1'])

            # These are not meant to be arguments.
            case 'id' | 'token_obtained_at' | 'token_expires_at', _:
                pass

            # Other subsections are not supported yet.
            case _, dict():
                print(f'Ignoring {key} because it is an unsupported dict')

            # Top-level config elements
            case _, _:
                arguments.extend(convert_to_arguments(None, key, value))

    return arguments


def get_priority_tag(priority: str) -> str:
    """
    Return the priority tag for a givne priority label.

    Nothing interesting, just deduplicating the logic.
    """
    return f'{PRIORITY_TAG_PREFIX}{priority}'


def get_runner_name(dut_name: str,
                    priority: str) -> str:
    """
    Return the standardized runner name for the given DUT's priority queue.
    """
    return f'{dut_name} - {get_priority_tag(priority)}'


def run(priority_config_path: pathlib.Path,
        gitlab_runner_args: list[str]):
    """
    Wrapper for gitlab-runner's `run` subcommand.

    This is where the magic happens; see README.md for the explanation.
    """
    must_stop = False
    # TODO: auto-detect priority-config changes and don't accept new jobs,
    #       and once nothing it running anymore, restart with the new config
    priority_config = get_priority_config(priority_config_path)
    runner_config = get_toml_config(get_runner_config_path(gitlab_runner_args))
    assert 'runners' in runner_config

    # Intercept SIGINT and SIGTERM to mean "quit cleanly as soon as possible"
    def handle_signals(sig, frame):
        match sig:
            case signal.SIGINT | signal.SIGTERM:
                nonlocal must_stop
                must_stop = True

    signal.signal(signal.SIGINT, handle_signals)
    signal.signal(signal.SIGTERM, handle_signals)

    duts_queues = collections.defaultdict(dict)
    for runner in runner_config['runners']:
        for priority in priority_config['priority_labels']:
            if not runner['name'].endswith(f' - {get_priority_tag(priority)}'):
                continue
            dut_name = runner['name'].rsplit(' - ', 1)[0]
            duts_queues[dut_name][priority] = runner

    def process_queues(queues: dict) -> None:
        while True:
            for priority in priority_config['priority_labels']:
                if must_stop:
                    return

                runner = queues[priority]
                print(f'Checking for DUT {runner["name"]} jobs')
                runner_args = get_arguments_for_runner(runner)

                run_single_command = [
                    'gitlab-runner', 'run-single',
                    '--max-builds', '1',
                    '--wait-timeout', str(priority_config['timeout_between_queues']),
                ]
                run_single_command += runner_args

                time_before_job = time.time()
                subprocess.check_call(run_single_command)
                time_after_job = time.time()
                job_duration = time_after_job - time_before_job

                # A job was picked up, restart the loop at the highest priority;
                # otherwise continue with the next priority
                if job_duration >= priority_config['min_job_duration']:
                    break

    with concurrent.futures.ThreadPoolExecutor(len(duts_queues)) as executor:
        futures = {
            executor.submit(process_queues, queues)
            for queues in duts_queues.values()
        }
    concurrent.futures.wait(futures)

    # The only expected reason to reach this point is if we were told to stop
    assert must_stop


def register(priority_config_path: pathlib.Path,
             url: str,
             registration_token: str,
             name: str,
             gitlab_runner_args: list[str]):
    """
    Wrapper for gitlab-runner's `register` subcommand, to register one runner
    per priority queue for each DUT.
    """
    priority_config = get_priority_config(priority_config_path)
    runner_config_path = get_runner_config_path(gitlab_runner_args)

    registered_runners = []
    try:
        for priority in priority_config['priority_labels']:
            priority_tag = get_priority_tag(priority)
            runner_name = get_runner_name(name, priority)
            register_command = [
                'gitlab-runner', 'register', '--non-interactive',
                '--config', runner_config_path,
                '--url', url,
                '--registration-token', registration_token,
                '--name', runner_name,
                '--tag-list', priority_tag,
                '--limit', '1',
                '--executor', 'docker',
                '--docker-image', '_',
            ] + gitlab_runner_args

            subprocess.check_call(register_command)
            registered_runners.append(runner_name)
    except subprocess.CalledProcessError:
        for runner in registered_runners:
            unregister(priority_config_path=priority_config_path,
                       name=runner,
                       gitlab_runner_args=[])


def unregister(priority_config_path: pathlib.Path,
               name: str,
               gitlab_runner_args: list[str]):
    """
    Wrapper for gitlab-runner's `unregister` subcommand, to handle the fact we
    have one runner per priority queue for each DUT.
    """
    priority_config = get_priority_config(priority_config_path)

    runner_config = get_toml_config(get_runner_config_path(gitlab_runner_args))

    for priority in priority_config['priority_labels']:
        runner_name = get_runner_name(name, priority)
        for runner in runner_config['runners']:
            if runner['name'] != runner_name:
                continue

            unregister_command = [
                'gitlab-runner', 'unregister',
                '--name', runner_name,
                '--url', runner['url'],
                '--token', runner['token'],
            ]
            subprocess.check_call(unregister_command)


def main():
    """
    Wrapper around gitlab-runner's `register`, `unregister`, and `run`
    subcommands.
    """
    default_priority_config_path = \
        get_gitlab_runner_config_folder() / 'priority_config.toml'

    parser = argparse.ArgumentParser()
    parser.add_argument('--priority-config',
                        type=pathlib.Path,
                        dest='priority_config_path',
                        default=default_priority_config_path,
                        help='Priority configuration file (default: '
                             f'{default_priority_config_path})')

    subparser = parser.add_subparsers(dest='subcommand')
    subcommand_run = subparser.add_parser('run')
    subcommand_register = subparser.add_parser('register')
    subcommand_unregister = subparser.add_parser('unregister')

    subcommand_register.add_argument('--url',
                                     required=True)
    subcommand_register.add_argument('--registration-token',
                                     required=True)
    subcommand_register.add_argument('--name',
                                     required=True)

    subcommand_unregister.add_argument('--name',
                                       required=True)

    args, gitlab_runner_args = parser.parse_known_args()

    args = vars(args)
    args['gitlab_runner_args'] = gitlab_runner_args
    subcommand = args.pop('subcommand')

    match subcommand:
        case 'run':
            run(**args)
        case 'register':
            register(**args)
        case 'unregister':
            unregister(**args)
        case _:
            parser.error('invalid subcommand')


if __name__ == '__main__':
    main()
